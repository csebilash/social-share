# Such Social Share
### Provide usefull functions for social sharing and mail
---

## Content

- [Installation](#installation)
- [With require](#with-require)
- [facebookShare](#facebookshare)
- [twitterShare](#twittershare)
- [mailTo](#mailto)
- [mandrillSend](#mandrillsend)

## Installation

With npm command :

    $ npm install such-social-share --save

## With require

On your js file :

    var Sshare = require('such-social-share')

## facebookShare

```javascript
var Sshare = require('such-social-share');

// Send directly a facebook share
Sshare.facebookShare();
```

## twitterShare

```javascript
var Sshare = require('such-social-share');
var message = "Share this";

// Send directly a twitter share with message
Sshare.twitterShare(message);
```

## mailTo

```javascript
var Sshare = require('such-social-share');
var email = "mymail@mysite.com";
var message = "Say my name <br> heisenDoge";
var subject = "Mail from contact";

// Send directly a twitter share with message
Sshare.mailTo(email, subject, message);
```

- email : email for the mailto, contact@mysite.com
- subject : subject for mail
- message : your message, you can use <br>

## mandrillSend

```javascript
var obj = {
    key : 'YourApiKey',
    from_email : 'sender.mail@mail.com',
    email : 'receiver.mail@mail.com',
    name : 'User Name',
    type : 'to',
    subject : 'hello you',
    content : 'test mail <br> blabla'
};
Sshare.mandrillSend(obj, function(data){
    console.log(data);
});
```