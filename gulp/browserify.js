'use strict';

var gulp        = require('gulp');
var gutil       = require('gulp-util');

// Browserify
var source     = require('vinyl-source-stream'); // Used to stream bundle for further handling
var buffer     = require('vinyl-buffer');
var browserify = require('browserify');
var watchify   = require('watchify');
var uglify     = require('gulp-uglify');
var babelify   = require('babelify');
var rename     = require('gulp-rename');

gulp.task('browserify-dev', function() {

    var bundler = browserify({
        entries: ['./index.js'],
        debug: true,
        standalone : 'Sshare',
        cache: {}, packageCache: {}, fullPaths: true
    }).transform(babelify);

    var watcher  = watchify(bundler);

    return watcher
        .on('update', function () {
            var updateStart = Date.now();
            console.log('Updating!');
            watcher.bundle()
                .on('error', gutil.log.bind(gutil, 'Browserify Error', gutil.colors.red('411')))
                .pipe(source('./index.js'))
                .pipe(buffer())
                .on('error', gutil.log)
                .pipe(gulp.dest('./bundle.js'));
            console.log('Updated!', (Date.now() - updateStart) + 'ms');
        })
        .bundle() // Cré le bundle initial lors du lancement de la commande
        .on('error', gutil.log.bind(gutil, 'Browserify Error', gutil.colors.red('411')))
        .pipe(source('./index.js'))
        .pipe(gulp.dest('./bundle.js'));
});

gulp.task('browserify-dist', function() {

    var bundler = browserify({
        insertGlobals: true,
        entries: ['./index.js'],
        debug: false,
        read : false,
        standalone : "Sshare",
        cache: {}, packageCache: {}, fullPaths: true
    }).transform(babelify);

    bundler
        .bundle()
        .on('error', gutil.log.bind(gutil, 'Browserify Error', gutil.colors.red('411')))
        .pipe(source('./index.js'))
        .pipe(buffer())
        .pipe(rename("sshare.min.js"))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'));
});